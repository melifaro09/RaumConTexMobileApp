[screen_login]:		screenshots/screen01.png "Login activity"
[screen_user]:      screenshots/screen02.png "Setup body feel values"
[screen_settings]:	screenshots/screen03.png "Personal user parameters"
[screen_expert_1]:	screenshots/screen04.png "PID control"
[screen_expert_2]:	screenshots/screen05.png "Heat circuit control"


# RaumConTex mobile
*RaumConTex mobile* is an Android application for controlling the heating table, which was developed as a part of the RaumConTex project (https://www.raumcontex.de). The application allows to customize the heating table, based on a "*feel*"-value of various body parts according to the norm ISO 10551:1995 "Ergonomics of the thermal environment" (German version EN ISO 10551:2001). The *expert mode* provides the ability for direct configuration of the PID-controller of SPS and other advanced settings.

## User view
![Setup body feel][screen_user]

The *User view* displays human model with 16 selectable zones (according ISO 9920:2007 "Ergonomics of the thermal environment", German version EN ISO 9920:2009). It is possible to set a value for each zone from "*too hot*" to "*too cold*", which are then used to calculate the necessary environment (respectively heating surface) temperature.

#### Settings tab
![User settings][screen_settings]

The *Settings*-tab contains personal user parameters according ISO 10551:1995. Here it refers to person height, clothing type and person activity level.

## Expert view
![Expert mode][screen_expert_1]
![Expert mode][screen_expert_2]

The *Expert view* contains advanced settings for the PID controller and heating circuits, and displays temperature information (current and expected) of heating circuits. The setting includes:

- Set up of `P`, `I`, `D`-parts of the PID-controller
- *Hysteresis*-factor of the PID
- *Desired temperatur* of heating circuit #1 and #2
- Switch on/off heating circuit #1 and #2
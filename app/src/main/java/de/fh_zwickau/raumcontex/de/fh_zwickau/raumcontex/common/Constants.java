package de.fh_zwickau.raumcontex.de.fh_zwickau.raumcontex.common;

/**
 * Global common constants
 *
 * Created by Alexandr Mitiaev on 17.05.2016.
 */
public class Constants {
    /**
     * Logs tag
     */
    public static final String LOG_TAG                  =       "RaumConTex";

    /**
     * Preferences name to get {@link android.content.SharedPreferences} instance
     */
    public static final String PREFS_NAME               =       "RaumConTex";

    /**
     * Extra parameter "server name"
     */
    public static final String EXTRA_PARAM_SERVER       =       "server";

    /**
     * Extra parameter "list of a servers"
     */
    public static final String EXTRA_PARAM_SERVER_LIST  =       "server_list";

    /**
     * Extra parameter "action"
     */
    public static final String EXTRA_PARAM_ACTION       =       "action";

    /**
     * Extra parameter "login"
     */
    public static final String EXTRA_PARAM_LOGIN        =       "login";

    /**
     * Extra parameter "password"
     */
    public static final String EXTRA_PARAM_PASSWORD     =       "password";

    /**
     * Extra parameter "feel value"
     */
    public static final String EXTRA_PARAM_FEEL         =       "feel";

    /**
     * Extra parameter "selection"
     */
    public static final String EXTRA_PARAM_SELECTION    =       "selection";

    /**
     * Extra parameter "person height"
     */
    public static final String EXTRA_PARAM_HEIGHT       =       "person_height";

    /**
     * Extra parameter "clothes type"
     */
    public static final String EXTRA_PARAM_CLOTHES      =       "clothes_type";

    /**
     * Extra parameter "activity type"
     */
    public static final String EXTRA_PARAM_ACTIVITY     =       "activity_type";

    /**
     * Extra parameter "number of a control circuits"
     */
    public static final String EXTRA_PARAM_CCNUMBER     =       "cc_number";

    /**
     * Extra parameter "server status"
     */
    public static final String EXTRA_PARAM_SERVER_STAT  =       "SERVER_STATUS";

    /**
     * Extra parameter "request result"
     */
    public static final String EXTRA_REQ_RESULT         =       "result";

    /**
     * Server command "unknown"
     */
    public static final byte COMMAND_UNKNOWN            =       0x00;

    /**
     * Server command "Authentication result"
     */
    public static final byte COMMAND_AUTH_RESULT        =       0x01;

    /**
     * Server command "Get sensors data"
     */
    public static final byte COMMAND_GET_SENSORS	    =		0x02;

    /**
     * Server command "Change Heizkreis1 status"
     */
    public static final byte COMMAND_HK1_CHANGE_STATE   =       0x03;

    /**
     * Server command "Change Heizkreis2 status"
     */
    public static final byte COMMAND_HK2_CHANGE_STATE   =       0x04;

    /**
     * Server command "Get input parameters"
     */
    public static final byte COMMAND_GET_INPUT_PARAMS   =       0x05;

    /**
     * Server command "Set input parameters"
     */
    public static final byte COMMAND_SET_INPUT_PARAMS   =       0x06;

    /**
     * Server command "Get room temperatur"
     */
    public static final byte COMMAND_GET_RAUM_TEMP      =       0x07;

    /**
     * Set feel value of a body part
     */
    public static final byte COMMAND_SET_FEEL           =	    0x0F;

    /**
     * Message code "Socket connected"
     */
    public static final int SOCKET_SERVER_CONNECTED     =       0x01;

    /**
     * Message code "Auth failed"
     */
    public static final int SOCKET_SERVER_AUTHFAILED    =       0x02;

    /**
     * Message code "Server unavailable"
     */
    public static final int SOCKET_SERVER_UNAVAILABLE   =       0x03;

    /**
     * Message code "No available servers"
     */
    public static final int SOCKET_NO_SERVERS           =       0x04;

    /**
     * Message code "Canceled by user"
     */
    public static final int SOCKET_USER_CANCELED        =       0x05;

    /**
     * Code for common socket error
     */
    public static final int SOCKET_ERROR                =       0x06;

    /**
     * Server answer: success
     */
    public static final byte RESULT_OK                  =       0x10;

    /**
     * Server answer: failed
     */
    public static final byte RESULT_FAILED              =       0x11;

    /**
     * Server answer: no permissions for a command
     */
    public static final byte RESULT_NO_PERMISSIONS      =       0x11;
}

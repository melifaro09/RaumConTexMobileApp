package de.fh_zwickau.raumcontex.de.fh_zwickau.raumcontex.common;

/**
 * Global configuration constants
 *
 * Created by Alexandr Mitiaev on 17.05.2016.
 */
public class Config {
    /**
     * Server IP address
     */
    public static final String SERVER_ADDR                  =       "93.104.210.158";

    /**
     * Default server port
     */
    public static final int SERVER_PORT                     =       9999;

    /**
     * Interval for reading sensor values (default: 2000 ms)
     */
    public static final int READING_INVERVALL               =       2000;

    /**
     * Format string for displaying a temperatur
     */
    public static final String FORMAT_STRING_TEMPERATUR     =       "%.1f °C";

    /**
     * Default value of temperatur-labels
     */
    public static final String FORMAT_DEF_TEMPERATUR_VALUE  =       "- °C";
}

package de.fh_zwickau.raumcontex.de.fh_zwickau.raumcontex.common;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.WindowManager;

/**
 * Progress and confirmation dialogs
 *
 * Created by Alexandr Mitiaev on 26.10.2016.
 */
public class DialogUtils {
    /**
     * The {@link ProgressDialog} instance
     */
    private static ProgressDialog mWaitDialog;

    /**
     * Wait dialog started-flag
     */
    private static boolean started = false;

    /**
     * Listener for choice-dialogs
     */
    public interface ChoiceDialogCallback {
        /**
         * Callback function for a positive result
         *
         * @param selectedIndex Index of selected element in list
         */
        public void onPositiveResult(int selectedIndex);

        /**
         * Callback function for a negative result
         */
        public void onNegativeResult();
    }

    /**
     * Displays the {@link ProgressDialog} with a given message
     *
     * @param aMessage Dialog message
     */
    public static void showWaitDialog(Context context, String aMessage) {
        mWaitDialog = new ProgressDialog(context);
        mWaitDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mWaitDialog.setIndeterminate(true);
        mWaitDialog.setCanceledOnTouchOutside(false);
        mWaitDialog.setMessage(aMessage);
        mWaitDialog.show();
        started = true;
    }

    /**
     * Display the {@link ProgressDialog} with a given message
     *
     * @param aMessage Message to display on dialog
     */
    public static void showWaitDialog(Context context, int aMessage) {
        showWaitDialog(context, context.getString(aMessage));
    }

    /**
     * Dismiss the {@link #mWaitDialog}
     */
    public static void dismissDialog() {
        if (started) {
            mWaitDialog.dismiss();
        }

        started = false;
    }

    /**
     * Stub listener for dialogs
     */
    private static final DialogInterface.OnClickListener mStubListener
            = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
        }
    };

    /**
     * Confirmation dialog box
     *
     * @param context The current {@link Context}
     * @param title Dialog title
     * @param message Dialog message
     * @param positiveListener The listener for "Yes" selection. If null, stub-listener will be used
     * @param negativeListener The listener for "No" selection. If null, stub-listener will be used
     */
    public static void showConfirmDialog(Context context, String title, String message,
                                         DialogInterface.OnClickListener positiveListener,
                                         DialogInterface.OnClickListener negativeListener,
                                         int positiveButton, int negativeButton) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle(title);
        builder.setMessage(message);

        if (positiveListener == null) {
            builder.setPositiveButton(context.getString(positiveButton), mStubListener);
        } else {
            builder.setPositiveButton(context.getString(positiveButton), positiveListener);
        }

        if (negativeListener == null) {
            builder.setNegativeButton(context.getString(negativeButton), mStubListener);
        } else {
            builder.setNegativeButton(context.getString(negativeButton), negativeListener);
        }

        AlertDialog alert = builder.create();
        alert.show();
    }

    /**
     * Show confirmation dialog box
     *
     * @param context The current {@link Context}
     * @param title Dialog title
     * @param message Dialog message
     * @param positiveListener The listener for "Yes" selection. If null, stub-listener will be used
     * @param negativeListener The listener for "No" selection. If null, stub-listener will be used
     */
    public static void showConfirmDialog(Context context, int title, int message,
                                         DialogInterface.OnClickListener positiveListener,
                                         DialogInterface.OnClickListener negativeListener,
                                         int positiveButton, int negativeButton) {
        showConfirmDialog(context, context.getString(title), context.getString(message),
                positiveListener, negativeListener, positiveButton, negativeButton);
    }

    /**
     * Show single-choice dialog
     *
     * @param context The {@link Context}
     * @param title   The dialog title
     * @param items   List of dialog items
     */
    public static void showSingleChoiceDialog(final ChoiceDialogCallback callback, Context context, String title, String [] items, boolean systemDialog) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);

        builder.setItems(items, null);

        final AlertDialog dialog = builder.create();

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dlg, int which) {
                if (callback != null) {
                    callback.onPositiveResult(dialog.getListView().getSelectedItemPosition());
                }
            }
        });

        if (systemDialog) {
            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        }

        dialog.show();
    }

    /**
     * Show radiobutton-choice dialog
     *
     * @param context The {@link Context}
     * @param title   The dialog title
     * @param items   List of dialog items
     */
    public static void showRadioButtonChoiceDialog(final ChoiceDialogCallback callback, Context context, String title, String [] items, boolean systemDialog) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);

        //final AlertDialog dialog = builder.create();

        builder.setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (callback != null) {
                    callback.onPositiveResult(((AlertDialog)dialog).getListView().getCheckedItemPosition());
                }
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (callback != null) {
                    callback.onNegativeResult();
                }
            }
        });

        final AlertDialog dialog = builder.create();

        if (systemDialog) {
            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        }

        dialog.show();
    }
}

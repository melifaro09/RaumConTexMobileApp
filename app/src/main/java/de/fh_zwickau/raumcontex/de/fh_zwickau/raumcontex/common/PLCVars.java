package de.fh_zwickau.raumcontex.de.fh_zwickau.raumcontex.common;

/**
 * Names of PLC-variables
 * (import from `TempControl`-module)
 * 
 * Created by Alexandr Mitiaev on 02.08.2017.
 */
public class PLCVars {
    /**
     * Name of the Vorlauf-variable of Heizkreis 1 in PLC
     */
    public static final String HK1_VORLAUF				=		"Main.hk1Vorlauf";
    
    /**
     * Name of the Pumpe-variable of Heizkreis 1 in PLC
     */
    public static final String HK1_PUMPE 				=		"Main.hk1Pumpe";
    
    /**
     * Name of the Mischer-variable of Heizkreis 1 in PLC
     */
    public static final String HK1_MISHER				=		"Main.hk1Mischer";
    
    /**
     * Name of the Raumteiler 1 variable of Heizkreis 1 in PLC
     */
    public static final String HK1_RAUMTEILER1			=		"Main.rt1";
    
    /**
     * Name of the Raumteiler 2 variable of Heizkreis 1 in PLC
     */
    public static final String HK1_RAUMTEILER2			=		"Main.rt2";
    
    /**
     * Name of the Raumteiler 3 variable of Heizkreis 1 in PLC
     */
    public static final String HK1_RAUMTEILER3			=		"Main.rt3";
    
    /**
     * Name of the Vorlauf-variable of Heizkreis 2 in PLC
     */
    public static final String HK2_VORLAUF				=		"Main.hk2Vorlauf";
    
    /**
     * Name of the Pumpe-variable of Heizkreis 2 in PLC
     */
    public static final String HK2_PUMPE				=		"Main.hk2Pumpe";
    
    /**
     * Name of the Misher-variable of Heizkreis 2 in PLC
     */
    public static final String HK2_MISHER				=		"Main.hk2Mischer";
    
    /**
     * Name of the Schreibtisch 1 variable of Heizkreis 2 in PLC
     */
    public static final String HK2_SCHREIBTISCH1		=		"Main.oberflaecheSchreibtisch1";
    
    /**
     * Name of the Schreibtisch 2 variable of Heizkreis 2 in PLC
     */
    public static final String HK2_SCHREIBTISCH2		=		"Main.oberflaecheSchreibtisch2";
    
    /**
     * Name of the Teppich 1 variable of Heizkreis 2 in PLC
     */
    public static final String HK2_TEPPICH1				=		"Main.oberflaecheTeppich1";
    
    /**
     * Name of the Teppich 2 variable of Heizkreis 2 in PLC
     */
    public static final String HK2_TEPPICH2				=		"Main.oberflaecheTeppich2";
    
    /**
     * Name of the Teppich 3 variable of Heizkreis 2 in PLC
     */
    public static final String HK2_TEPPICH3				= 		"Main.oberflaecheTeppich3";
    
    /**
     * Name of the Vorlauf-variable of Steinspeicher in PLC
     */
    public static final String SS_VORLAUF				=		"Main.sspVorlauf";
    
    /**
     * Name of the Pumpe-variable of Steinspeicher in PLC
     */
    public static final String SS_PUMPE					=		"Main.sspPumpe";
    
    /**
     * Steinspeicher umschaltswentil
     */
    public static final String SS_UMSCHALTVENTIL		=		"Main.sspUSV";
	
	/**
     * Name of the Temperatur-variable of Steinspeicher in PLC
     */
	public static final String SS_TEMP					=		"Main.sspTemp";
	
	/**
	 * Raumtemperatur
	 */
	public static final String RAUM_TEMP				=		"Main.raumTemp";

	/**
     * Name of the VorlaufSoll-variable of Heizkreis 1 in PLC
     */
	public static final String HK1_VORLAUF_SOLL			=		"Main.hk1VorlaufSoll";
	
	/**
     * Name of the Kp-factor-variable of Heizkreis 1 in PLC
     */
	public static final String HK1_KP					=		"Main.hk1Kp";
	
	/**
     * Name of the Ki-factor-variable of Heizkreis 1 in PLC
     */
	public static final String HK1_KI					=		"Main.hk1Ki";
	
	/**
     * Name of the Kd-factor-variable of Heizkreis 1 in PLC
     */
	public static final String HK1_KD					=		"Main.hk1Kd";
	
	/**
     * Name of the VorlaufSoll-variable of Heizkreis 2 in PLC
     */
	public static final String HK2_VORLAUF_SOLL			=		"Main.hk2VorlaufSoll";
	
	/**
     * Name of the Kp-factor-variable of Heizkreis 2 in PLC
     */
	public static final String HK2_KP					=		"Main.hk2Kp";
	
	/**
     * Name of the Ki-factor-variable of Heizkreis 2 in PLC
     */
	public static final String HK2_KI					=		"Main.hk2Ki";
	
	/**
     * Name of the Kd-factor-variable of Heizkreis 2 in PLC
     */
	public static final String HK2_KD					=		"Main.hk2Kd";
	
	/**
     * Name of the VorlaufSoll-variable of Steinspeicher in PLC
     */
	public static final String SS_VORLAUF_SOLL			=		"Main.sspSollwert";
	
	/**
     * PIDs hysteresis value
     */
	public static final String SS_HYSTERESE		=				"Main.sspHysterese";
		
	/**
     * PLCs Initialized-flag
     */
	public static final String INITIALISED				=		"Main.initialisiert";
	
	/**
	 * PLC manual mode
	 */
	public static final String MANUAL_MODE				=		"Main.Handsteuerung";
	
	/**
	 * Close Heizkreis1 mixer
	 */
	public static final String HK1_MISCHER_AUF			=		"Main.hk1MischerAuf";
	
	/**
	 * Close Heizkreis1 mixer
	 */
	public static final String HK1_MISCHER_ZU			=		"Main.hk1MischerZu";
	
	/**
	 * Open Heizkreis2 mixer
	 */
	public static final String HK2_MISCHER_AUF			=		"Main.hk2MischerAuf";
	
	/**
	 * Close Heizkreis2 mixer
	 */
	public static final String HK2_MISCHER_ZU			=		"Main.hk2MischerZu";
	
	/**
	 * Heizkreis 1 on/off
	 */
	public static final String HK1_ON					=		"Main.hk1On";
	
	/**
	 * Heizkreis 2 on/off
	 */
	public static final String HK2_ON					=		"Main.hk2On";
	
	/**
	 * PLC mode: true = warm, false = cool
	 */
	public static final String HEATING_MODE				=		"Main.heatingMode";
	
	/**
	 * Calculated tau-point value
	 */
	public static final String TAUPUNKT					=		"Main.tpt";
	
	/**
	 * Room temperatur for calculation of the tau-point
	 */
	public static final String TEMPERATUR_TPT			=		"Main.temperatur";
	
	/**
	 * Phi-value for calculation of the tau-point
	 */
	public static final String PHI						=		"Main.phi";
}

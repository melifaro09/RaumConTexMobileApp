package de.fh_zwickau.raumcontex.network;

/**
 * Network command constants
 *
 * Created by Alexandr on 28.08.2017.
 */
public class NetworkActions {
    /**
     * Extra parameter name for network actions
     */
    public static final String NET_ACTION             =               "NET_ACTION";

    /**
     * Perform user authentification
     */
    public static final String NET_LOGIN              =               "NET_LOGIN";

    /**
     * Set body part feel value
     */
    public static final String NET_SET_FEEL           =               "NET_SET_FEEL";
}

package de.fh_zwickau.raumcontex.network;

import de.fh_zwickau.raumcontex.de.fh_zwickau.raumcontex.common.Constants;

/**
 * Wrapper for server-side errors/exceptions
 *
 * Created by Alexandr Mitiaev on 02.08.2017.
 */

public class NetworkServiceException extends Exception {
    /**
     * The result/error code
     */
    private byte resultCode;

    /**
     * Create new {@link NetworkServiceException} with a given
     * error code, received from the server
     *
     * @param resultCode
     */
    public NetworkServiceException(byte resultCode) {
        this.resultCode = resultCode;
    }

    /**
     * Returns server error code
     *
     * @return One from Constants.RESULT_* constants
     */
    public byte getErrorCode() {
        switch (resultCode) {
            case 1:
                return Constants.RESULT_OK;
            case 2:
                return Constants.RESULT_FAILED;
            case 3:
                return Constants.RESULT_NO_PERMISSIONS;
            default:
                return Constants.SOCKET_ERROR;
        }
    }
}

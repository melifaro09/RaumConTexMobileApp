package de.fh_zwickau.raumcontex.network;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

import de.fh_zwickau.raumcontex.de.fh_zwickau.raumcontex.common.Config;
import de.fh_zwickau.raumcontex.de.fh_zwickau.raumcontex.common.Constants;
import de.fh_zwickau.raumcontex.de.fh_zwickau.raumcontex.common.PLCVars;

/**
 * Network service for server connection
 *
 * Created by Alexandr Mitiaev on 02.08.2017.
 */
public class NetworkService extends Service {

    /**
     * Broadcast action string for sending data
     */
    public static final String BROADCAST_ACTION_SEND    =       "de.fh_zwickau.raumconext.netevent";

    /**
     * Broadcast action string for receiving data
     */
    public static final String BROADCAST_ACTION_RECEIVE =       "de.fh_zwickau.raumcontext.netsend";

    /**
     * The {@link Socket} to use
     */
    public Socket mSocket;

    /**
     * The {@link DataOutputStream}
     */
    private DataOutputStream out;

    /**
     * The {@link DataInputStream}
     */
    private DataInputStream in;

    /**
     * Authentification thread
     */
    private Thread authThread;

    /**
     * The {@link LocalBroadcastManager}
     */
    private LocalBroadcastManager mBroadcastManager;

    /**
     * Create service
     */
    @Override
    public void onCreate() {
        super.onCreate();

        try {
            mBroadcastManager = LocalBroadcastManager.getInstance(this);
            mBroadcastManager.registerReceiver(broadcastReceiver, new IntentFilter(BROADCAST_ACTION_RECEIVE));
        } catch (Exception e) {
            Log.e(Constants.LOG_TAG, e.getMessage());
        }
    }

    /**
     * Close the {@link #mSocket}
     */
    private void closeSocket() {
        try {
            if (mSocket != null && mSocket.isConnected()) {
                mSocket.close();
            }
        } catch (IOException e) {
            Log.e(Constants.LOG_TAG, e.getMessage());
        } finally {
            mSocket = null;
        }
    }

    /**
     * Destroy service
     */
    @Override
    public void onDestroy() {
        mBroadcastManager.unregisterReceiver(broadcastReceiver);
        closeSocket();
    }

    /**
     * Returns the {@link SharedPreferences} for the app
     *
     * @param mode Operating mode
     *
     * @return  The {@link SharedPreferences} instance
     */
    public SharedPreferences getPreferences(final int mode) {
        return getSharedPreferences(Constants.PREFS_NAME, mode);
    }

    /**
     * Start service: perform authentification
     *
     * @param intent
     * @param flags
     * @param startId
     *
     * @return
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent != null && intent.hasExtra(Constants.EXTRA_PARAM_SERVER)) {
            final int selectedServer = intent.getIntExtra(Constants.EXTRA_PARAM_SERVER, 0);

            try {
                Runnable th = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            out.writeInt(selectedServer);

                            byte sur_count = in.readByte();
                            String[] surfaces = new String[sur_count];

                            for (int i = 0; i < sur_count; i++) {
                                surfaces[i] = in.readUTF();
                            }

                            Intent response = new Intent(NetworkService.BROADCAST_ACTION_SEND);
                            response.putExtra("SURFACES", surfaces);
                            mBroadcastManager.sendBroadcast(response);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                };

                authThread = new Thread(th);
                authThread.start();

            } catch (Exception e) {
                Log.e(Constants.LOG_TAG, e.getMessage());
            }

            return super.onStartCommand(intent, flags, startId);
        }

        closeSocket();

        String login = "";
        String password = "";

        if (intent != null && intent.hasExtra(Constants.EXTRA_PARAM_LOGIN) && intent.hasExtra(Constants.EXTRA_PARAM_PASSWORD)) {
            login = intent.getStringExtra(Constants.EXTRA_PARAM_LOGIN);
            password = intent.getStringExtra(Constants.EXTRA_PARAM_PASSWORD);

            final SharedPreferences.Editor editor = getPreferences(MODE_PRIVATE).edit();
            editor.putString(Constants.EXTRA_PARAM_LOGIN, login);
            editor.putString(Constants.EXTRA_PARAM_PASSWORD, password);
        } else {
            final SharedPreferences pref = getPreferences(MODE_PRIVATE);
            login = pref.getString(Constants.EXTRA_PARAM_LOGIN, "");
            password = pref.getString(Constants.EXTRA_PARAM_PASSWORD, "");
        }

        performLogin(login, password);

        return super.onStartCommand(intent, flags, startId);
    }

    /**
     * Try to login on remote server
     *
     * @param login The login
     * @param password The password
     */
    private void performLogin(final String login, final String password) {
        final Runnable runnable = new Runnable() {
            public void run() {
                try {
                    try {
                        mSocket = new Socket(InetAddress.getByName(Config.SERVER_ADDR), Config.SERVER_PORT);

                        in = new DataInputStream(mSocket.getInputStream());
                        out = new DataOutputStream(mSocket.getOutputStream());
                    } catch (Exception e) {
                        Log.e(Constants.LOG_TAG, e.getMessage());
                        broadcastServerStatus(Constants.SOCKET_SERVER_UNAVAILABLE);
                        mSocket = null;
                        return;
                    }

                    out.writeByte(0);       // Client type: 0 - normal client
                    out.writeUTF(login);
                    out.writeUTF(password);

                    out.flush();

                    final byte authResult = in.readByte();

                    if (authResult == 0) {
                        broadcastServerStatus(Constants.SOCKET_SERVER_AUTHFAILED);
                        mSocket.close();
                        mSocket = null;
                    } else {
                        final int serversNumber = in.readByte();

                        if (serversNumber > 0) {
                            String[] items = new String[serversNumber];

                            for (int i = 0; i < serversNumber; i++) {
                                items[i] = in.readUTF();
                            }

                            final Intent response = new Intent(NetworkService.BROADCAST_ACTION_SEND);
                            response.putExtra(Constants.EXTRA_PARAM_SERVER_LIST, items);
                            response.putExtra(Constants.EXTRA_PARAM_SERVER_STAT, Constants.SOCKET_SERVER_CONNECTED);
                            mBroadcastManager.sendBroadcast(response);
                        } else {
                            broadcastServerStatus(Constants.SOCKET_NO_SERVERS);
                        }
                    }
                } catch (Exception e) {
                    Log.e(Constants.LOG_TAG, e.getMessage());
                    broadcastServerStatus(Constants.SOCKET_SERVER_UNAVAILABLE);
                }
            }
        };

        final Thread thread = new Thread(runnable);
        thread.start();
    }

    /**
     * Broadcast the given server status
     *
     * @param statusCode The server status code
     */
    private void broadcastServerStatus(int statusCode) {
        try {
            final Intent response = new Intent(NetworkService.BROADCAST_ACTION_SEND);
            response.putExtra(Constants.EXTRA_PARAM_SERVER_STAT, statusCode);
            mBroadcastManager.sendBroadcast(response);
        } catch (Exception e) {
            Log.e(Constants.LOG_TAG, e.getMessage());
        }
    }

    /**
     * Data from activity
     */
    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        /**
         * Mutex
         */
        private Object lock = new Object();

        /**
         * Receive data from client
         */
        @Override
        public void onReceive(final Context context, final Intent intent) {

            synchronized (lock) {

                final byte action = intent.getByteExtra(Constants.EXTRA_PARAM_ACTION, Constants.COMMAND_UNKNOWN);

                final Intent response = new Intent(NetworkService.BROADCAST_ACTION_SEND);
                response.putExtra(Constants.EXTRA_PARAM_ACTION, action);

                if (mSocket == null || !mSocket.isConnected()) {
                    response.putExtra("result", Constants.RESULT_FAILED);
                    mBroadcastManager.sendBroadcast(response);
                    return;
                }

                Runnable runnable = new Runnable() {
                    public void run() {
                        synchronized (lock) {
                            switch (action) {
                                case Constants.COMMAND_GET_SENSORS:
                                    loadSensorData(response);
                                    break;

                                case Constants.COMMAND_GET_INPUT_PARAMS:
                                    loadServerParams(response);
                                    break;

                                case Constants.COMMAND_SET_INPUT_PARAMS:
                                    try {
                                        sendServerParams(intent);
                                    } catch (NetworkServiceException e) {
                                        response.putExtra("result", e.getErrorCode());
                                    }
                                    break;

                                case Constants.COMMAND_HK1_CHANGE_STATE:
                                    try {
                                        boolean hk1On = readBoolean("Main.hk1On");
                                        writeBoolean("Main.hk1On", !hk1On);
                                    } catch (NetworkServiceException e) {
                                        response.putExtra("result", e.getErrorCode());
                                    }
                                    break;

                                case Constants.COMMAND_HK2_CHANGE_STATE:
                                    try {
                                        boolean hk2On = readBoolean("Main.hk2On");
                                        writeBoolean("Main.hk2On", !hk2On);
                                    } catch (NetworkServiceException e) {
                                        response.putExtra("result", e.getErrorCode());
                                    }
                                    break;

                                case Constants.COMMAND_GET_RAUM_TEMP:
                                    try {
                                        float raumTemp = readFloat(PLCVars.RAUM_TEMP);
                                        response.putExtra("result", Constants.RESULT_OK);
                                        response.putExtra(PLCVars.RAUM_TEMP, raumTemp);
                                    } catch (NetworkServiceException e) {
                                        response.putExtra("result", e.getErrorCode());
                                    }
                                    break;

                                case Constants.COMMAND_SET_FEEL:
                                    setFeelValue(intent, response);
                                    break;
                            }

                            mBroadcastManager.sendBroadcast(response);
                        }
                    }
                };

                Thread thread = new Thread(runnable);
                thread.start();
            }
        }
    };

    /**
     * Send "set feel" request to the server
     *
     * @param request
     * @param response
     */
    private void setFeelValue(Intent request, Intent response) {
        try {
            out.writeByte(Constants.COMMAND_SET_FEEL);

            int permissions = in.readByte();

            if (permissions == 0x03) {
                response.putExtra(Constants.EXTRA_REQ_RESULT, Constants.RESULT_NO_PERMISSIONS);
            } else {
                int feel = request.getIntExtra(Constants.EXTRA_PARAM_FEEL, -1);
                out.writeInt(feel);

                int index = request.getIntExtra(Constants.EXTRA_PARAM_SELECTION, -1);
                out.writeInt(index);

                int val = request.getIntExtra(Constants.EXTRA_PARAM_HEIGHT, -1);
                out.writeInt(val);

                val = request.getIntExtra(Constants.EXTRA_PARAM_CLOTHES, -1);
                out.writeInt(val);

                val = request.getIntExtra(Constants.EXTRA_PARAM_ACTIVITY, -1);
                out.writeInt(val);

                int rk_count = in.readInt();
                response.putExtra(Constants.EXTRA_PARAM_CCNUMBER, rk_count);

                for (int i = 0; i < rk_count; i++) {
                    double rk_before = in.readInt() / 10.0;
                    double rk_after = in.readInt() / 10.0;
                    response.putExtra(String.format("rk_before%s", i), rk_before);
                    response.putExtra(String.format("rk_after%s", i), rk_after);
                }

                byte res = in.readByte();

                if (res == 0x01) {
                    response.putExtra(Constants.EXTRA_REQ_RESULT, Constants.RESULT_OK);
                } else if (res == 0x02) {
                    response.putExtra(Constants.EXTRA_REQ_RESULT, Constants.RESULT_FAILED);
                }
            }
        } catch (IOException e) {
            Log.e(Constants.LOG_TAG, e.getMessage());
            response.putExtra(Constants.EXTRA_REQ_RESULT, Constants.RESULT_FAILED);
        }
    }

    /**
     * Send HK and PID parameters to the server
     *
     * @param request
     */
    private void sendServerParams(final Intent request) throws NetworkServiceException {
        try {
            sendFloatValue(request, PLCVars.HK1_VORLAUF_SOLL);
            sendFloatValue(request, PLCVars.HK1_KI);
            sendFloatValue(request, PLCVars.HK1_KP);
            sendFloatValue(request, PLCVars.HK1_KD);
            sendFloatValue(request, PLCVars.HK2_VORLAUF_SOLL);
            sendFloatValue(request, PLCVars.HK2_KI);
            sendFloatValue(request, PLCVars.HK2_KP);
            sendFloatValue(request, PLCVars.HK2_KD);
            sendFloatValue(request, PLCVars.SS_VORLAUF_SOLL);
            sendFloatValue(request, PLCVars.SS_HYSTERESE);
            sendBooleanValue(request, PLCVars.HEATING_MODE);
        } catch (Exception e) {
            Log.e(Constants.LOG_TAG, e.getMessage());
        }
    }

    /**
     * Loading HK and PID parameters from the server
     *
     * @param response
     */
    private void loadServerParams(final Intent response) {
        try {
            loadFloatValue(response, PLCVars.HK1_VORLAUF_SOLL);
            loadFloatValue(response, PLCVars.HK1_KI);
            loadFloatValue(response, PLCVars.HK1_KP);
            loadFloatValue(response, PLCVars.HK1_KD);
            loadFloatValue(response, PLCVars.HK2_VORLAUF_SOLL);
            loadFloatValue(response, PLCVars.HK2_KI);
            loadFloatValue(response, PLCVars.HK2_KP);
            loadFloatValue(response, PLCVars.HK2_KD);
            loadFloatValue(response, PLCVars.SS_VORLAUF_SOLL);
            loadFloatValue(response, PLCVars.SS_HYSTERESE);
            loadBooleanValue(response, PLCVars.HEATING_MODE);

            response.putExtra(Constants.EXTRA_REQ_RESULT, Constants.RESULT_OK);
        } catch (NetworkServiceException e) {
            Log.e(Constants.LOG_TAG, e.getMessage());
            response.putExtra(Constants.EXTRA_REQ_RESULT, Constants.RESULT_FAILED);
        }
    }

    /**
     * Loading sensor information from the server and put it
     * in extra-parameters
     *
     * @param response
     */
    private void loadSensorData(final Intent response) {
        try {
            loadFloatValue(response, PLCVars.RAUM_TEMP);
            loadBooleanValue(response, PLCVars.HEATING_MODE);
            loadFloatValue(response, PLCVars.HK1_VORLAUF);
            loadFloatValue(response, PLCVars.HK2_VORLAUF);
            loadFloatValue(response, PLCVars.SS_VORLAUF);
            loadFloatValue(response, PLCVars.HK1_VORLAUF_SOLL);
            loadFloatValue(response, PLCVars.HK2_VORLAUF_SOLL);
            loadFloatValue(response, PLCVars.SS_VORLAUF_SOLL);
            loadBooleanValue(response, PLCVars.HK1_ON);
            loadBooleanValue(response, PLCVars.HK2_ON);

            response.putExtra(Constants.EXTRA_REQ_RESULT, Constants.RESULT_OK);
        } catch (NetworkServiceException e) {
            Log.e(Constants.LOG_TAG, e.getMessage());
            response.putExtra(Constants.EXTRA_REQ_RESULT, Constants.RESULT_FAILED);
        }
    }

    /**
     * Loading a boolean value from the server and put it as extra-value
     *
     * @param varName Name of the PLC variable
     */
    private void loadBooleanValue(final Intent intent, final String varName) throws NetworkServiceException {
        intent.putExtra(varName, readBoolean(varName));
    }

    /**
     * Loading a float value from the server and put it as extra-value
     *
     * @param varName Name of the PLC variable
     */
    private void loadFloatValue(final Intent intent, final String varName) throws NetworkServiceException {
        intent.putExtra(varName, readFloat(varName));
    }

    /**
     * Send a boolean value from extra-parameters to the PLC
     *
     * @param varName Name of the PLC variable
     */
    private void sendBooleanValue(final Intent intent, final String varName) throws NetworkServiceException {
        if (intent.hasExtra(varName)) {
            writeBoolean(varName, intent.getBooleanExtra(varName, false));
        }
    }

    /**
     * Send a float value from extra-parameters to the PLC
     *
     * @param varName Name of the PLC variable
     */
    private void sendFloatValue(final Intent intent, final String varName) throws NetworkServiceException {
        if (intent.hasExtra(varName)) {
            writeFloat(varName, intent.getFloatExtra(varName, 0));
        }
    }

    /**
     * Read float variable from Control Server
     *
     * @param value
     * @return
     */
    private float readFloat(String value) throws NetworkServiceException {
        float result = 0;

        try {
            out.writeByte(0x0C);        // COMMAND_READ_FLOAT_VALUE
            out.writeUTF(value);

            byte res = in.readByte();

            if (res == 0x01) { // RESULT_OK
                result = in.readFloat();
            } else {
                throw new NetworkServiceException(res);
            }

        } catch (IOException e) {
            Log.e(Constants.LOG_TAG, e.getMessage());
            throw new NetworkServiceException((byte)Constants.SOCKET_ERROR);
        }

        return result;
    }

    /**
     * Read boolean variable from Control Server
     *
     * @param value
     * @return
     */
    private boolean readBoolean(String value) throws NetworkServiceException {
        boolean result = false;

        try {
            out.writeByte(0x0D);        // COMMAND_READ_BOOLEAN_VALUE
            out.writeUTF(value);

            byte res = in.readByte();

            if (res == 0x01) {              // RESULT_OK
                result = in.readBoolean();
            } else {
                throw new NetworkServiceException(res);
            }

        } catch (IOException e) {
            Log.e(Constants.LOG_TAG, e.getMessage());
            throw new NetworkServiceException((byte)Constants.SOCKET_ERROR);
        }

        return result;
    }

    /**
     * Write boolean value to the Control Server
     *
     * @param varName Name of the variable to write
     * @param value Value to write
     *
     * @return
     */
    private byte writeBoolean(String varName, boolean value) throws NetworkServiceException {
        try {
            out.writeByte(0x0b);          // COMMAND_WRITE_BOOLEAN_VALUE
            out.writeUTF(varName);
            out.writeBoolean(value);

            byte res = in.readByte();         // Skip result
            if (res != 0x01) {
                throw new NetworkServiceException(res);
            }
        } catch (IOException e) {
            Log.e(Constants.LOG_TAG, e.getMessage());
            throw new NetworkServiceException((byte)Constants.SOCKET_ERROR);
        }

        return Constants.RESULT_FAILED;
    }

    /**
     * Send float value to the control server
     *
     * @param varName   Name of the variable
     * @param value     Variable value
     *
     * @return Result
     */
    private void writeFloat(final String varName, final float value) throws NetworkServiceException {
        try {
            out.writeByte(0x0a);
            out.writeUTF(varName);
            out.writeFloat(value);

            byte res = in.readByte();              // Skip result

            if (res != 0x01) {
                throw new NetworkServiceException(res);
            }
        } catch (IOException e) {
            Log.e(Constants.LOG_TAG, e.getMessage());
            throw new NetworkServiceException((byte)Constants.SOCKET_ERROR);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}

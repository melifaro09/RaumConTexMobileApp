package de.fh_zwickau.raumcontex;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import de.fh_zwickau.raumcontex.de.fh_zwickau.raumcontex.common.Constants;
import de.fh_zwickau.raumcontex.network.NetworkActions;
import de.fh_zwickau.raumcontex.network.NetworkService;
import de.fh_zwickau.raumcontex.de.fh_zwickau.raumcontex.common.DialogUtils;

/**
 * Login activity
 *
 * Created by Alexandr Mitiaev on 22.09.2016.
 */
public class LoginActivity extends AppCompatActivity implements DialogUtils.ChoiceDialogCallback {
    /**
     * The {@link LocalBroadcastManager}
     */
    private LocalBroadcastManager broadcastManager;

    /**
     * Login edit field
     */
    private EditText edLogin;

    /**
     * Password edit field
     */
    private EditText edPassword;

    /**
     * Radio button "Expert mode"
     */
    private RadioButton rbExpertMode;

    /**
     * Create activity
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edLogin = (EditText)findViewById(R.id.edLogin);
        edPassword = (EditText)findViewById(R.id.edPassword);
        rbExpertMode = (RadioButton)findViewById(R.id.rbExpertMode);

        // Stop service, if running
        stopService(new Intent(this, NetworkService.class));

        // Register broadcast receiver
        broadcastManager = LocalBroadcastManager.getInstance(this);
        broadcastManager.registerReceiver(broadcastReceiver, new IntentFilter(NetworkService.BROADCAST_ACTION_SEND));
    }

    /**
     * On destroy activity
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        broadcastManager.unregisterReceiver(broadcastReceiver);
    }

    /**
     * "Connect" button clicked
     *
     * @param v
     */
    public void onConnectClicked(View v) {
        String login = edLogin.getText().toString();
        String password = edLogin.getText().toString();

        if (login.isEmpty() || password.isEmpty()) {
            Toast.makeText(this, R.string.error_no_loginpass, Toast.LENGTH_LONG).show();
        } else {
            DialogUtils.showWaitDialog(this, R.string.msg_connect);

            Intent intent = new Intent(this, NetworkService.class);

            intent.putExtra(NetworkActions.NET_ACTION, NetworkActions.NET_LOGIN);
            intent.putExtra(Constants.EXTRA_PARAM_LOGIN, edLogin.getText().toString());
            intent.putExtra(Constants.EXTRA_PARAM_PASSWORD, edPassword.getText().toString());

            startService(intent);
        }
    }

    /**
     * Local {@link BroadcastReceiver}
     */
    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            DialogUtils.dismissDialog();

            if (intent.hasExtra("SURFACES")) {
                Intent userIntent = null;
                if (rbExpertMode.isChecked()) {
                    userIntent = new Intent(getBaseContext(), ExpertActivity.class);
                } else {
                    userIntent = new Intent(getBaseContext(), UserActivity.class);
                }

                userIntent.putExtra("SURFACES", intent.getStringArrayExtra("SURFACES"));
                startActivity(userIntent);
            }

            int serverStatus = intent.getIntExtra(Constants.EXTRA_PARAM_SERVER_STAT, 0);

            if (serverStatus == Constants.SOCKET_SERVER_CONNECTED) {
                DialogUtils.showRadioButtonChoiceDialog(getInstance(), getBaseContext(), "Wählen Sie die Anlage aus",
                        intent.getStringArrayExtra(Constants.EXTRA_PARAM_SERVER_LIST), false);
            } else if (serverStatus == Constants.SOCKET_NO_SERVERS) {
                Toast.makeText(getBaseContext(), R.string.msg_no_servers, Toast.LENGTH_LONG).show();
            } else if (serverStatus == Constants.SOCKET_SERVER_UNAVAILABLE) {
                Toast.makeText(getBaseContext(), R.string.msg_server_unavailable, Toast.LENGTH_LONG).show();
            } else if (serverStatus == Constants.SOCKET_SERVER_AUTHFAILED) {
                Toast.makeText(getBaseContext(), R.string.msg_invalid_loginpass, Toast.LENGTH_LONG).show();
            } else if (serverStatus == Constants.SOCKET_USER_CANCELED) {
                Toast.makeText(getBaseContext(), "Canceled", Toast.LENGTH_LONG).show();
            } else if (serverStatus == Constants.SOCKET_ERROR) {
                Toast.makeText(getBaseContext(), "Connection error", Toast.LENGTH_LONG).show();
            }
        }
    };

    /**
     * Returns the {@link LoginActivity} instance
     *
     * @return The {@link LoginActivity}
     */
    public LoginActivity getInstance() {
        return this;
    }

    @Override
    public void onPositiveResult(int selectedIndex) {
        Intent intent = new Intent(this, NetworkService.class);
        intent.putExtra(Constants.EXTRA_PARAM_SERVER, selectedIndex);

        startService(intent);

        Toast.makeText(this, R.string.msg_connected, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNegativeResult() {
        Toast.makeText(this, "Canceled", Toast.LENGTH_LONG).show();
    }
}

package de.fh_zwickau.raumcontex;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

import de.fh_zwickau.raumcontex.de.fh_zwickau.raumcontex.common.Config;
import de.fh_zwickau.raumcontex.de.fh_zwickau.raumcontex.common.Constants;
import de.fh_zwickau.raumcontex.network.NetworkService;
import de.fh_zwickau.raumcontex.de.fh_zwickau.raumcontex.common.PLCVars;
import de.fh_zwickau.raumcontex.de.fh_zwickau.raumcontex.common.DialogUtils;

/**
 * User activity: select feel values for body parts
 *
 * Created by Alexandr Mitiaev on 22.09.2016.
 */
public class UserActivity extends AppCompatActivity implements View.OnTouchListener {
    /**
     * List of colors for front zones [1..15] in model_map.png
     */
    private final List<Integer> ZONES_COLORS_FRONT = Arrays.asList(0xFF00c8c8, 0xFFc8c800, 0xFFb48c00, 0xFF780019, 0xFF000078, 0xFF007878, 0xFF007800, 0xFF787800, 0xFF780000, 0xFFb44100, 0xFF0000ff, 0xFF00ff00, 0xFF78a0b4, 0xFFff0000, 0xFFffff00);

    /**
     * List of colors for back zones [1..15] in model_map.png
     */
    private final List<Integer> ZONES_COLORS_BACK = Arrays.asList(0xFF0000c8, 0xFF00c800, 0xFFc80000, 0xFF780078, 0xFFb4dcbe, 0xFF006ebe, 0xFF14006e, 0xFF14af00, 0xFF78f096, 0xFFff00ff, 0xFFb464ff, 0xFF7823b4, 0xFF00ffff, 0xFF14c8ff, 0xFF145aff);

    /**
     * Number of zones on model
     */
    private final static int ZONES_NUMBER           =           16;

    /**
     * The {@link LocalBroadcastManager}
     */
    private LocalBroadcastManager broadcastManager;

    /**
     * Overlay image
     */
    private ImageView imgOverlay;

    /**
     * Image view for selected area
     */
    private ImageView imgSelection;

    /**
     * Modell image
     */
    private ImageView imgModell;

    /**
     * Hotspots bitmap
     */
    private Bitmap hotspots = null;

    /**
     * Feels {@link SeekBar}
     */
    private SeekBar sbFeel;

    /**
     * The {@link LinearLayout} with a {@link #sbFeel}
     */
    private LinearLayout llSpinner;

    /**
     * Raum temperatur
     */
    private TextView tvRaumTemp;

    /**
     * Index of selected zone
     */
    private int selectedIndex;

    /**
     * Selected feel value
     */
    private int feelValue;

    /**
     * Update thread
     */
    private Thread mReadingThread;

    /**
     * Update thread running
     */
    private boolean mRunning;

    /**
     *
     */
    private TabHost tbTabs;

    /**
     *
     */
    private Spinner sHeight;

    /**
     * Clothes list
     */
    private Spinner sClothes;

    /**
     * Activity list
     */
    private Spinner sActivity;

    /**
     * Apply-button
     */
    private Button btnApply;

    /**
     * OnCreate activity
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        imgOverlay = (ImageView)findViewById(R.id.imgOverlay);
        imgSelection = (ImageView)findViewById(R.id.imgSelection);

        sbFeel = (SeekBar)findViewById(R.id.sbFeel);
        llSpinner = (LinearLayout)findViewById(R.id.llSpinner);
        tvRaumTemp = (TextView)findViewById(R.id.tvRaumTemp);
        btnApply = (Button)findViewById(R.id.btnApply);

        llSpinner.setVisibility(View.INVISIBLE);

        selectedIndex = -1;

        sbFeel.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (selectedIndex != -1) {
                    feelValue = i;
                    btnApply.setEnabled(true);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        initTabs();
        initSpinners();

        imgModell = (ImageView)findViewById(R.id.imgModell);
        imgModell.setOnTouchListener(this);

        // Register broadcast receiver
        broadcastManager = LocalBroadcastManager.getInstance(this);
        broadcastManager.registerReceiver(broadcastReceiver, new IntentFilter(NetworkService.BROADCAST_ACTION_SEND));

        // Start update temperatur cycle
        startUpdateThread();
    }

    /**
     * Initialize tabs
     */
    private void initTabs() {
        tbTabs = (TabHost)findViewById(R.id.tbTabs);
        tbTabs.setup();

        TabHost.TabSpec tab = tbTabs.newTabSpec("tab1");
        tab.setIndicator(getString(R.string.caption_tab_feel));
        tab.setContent(R.id.llCommon);
        tbTabs.addTab(tab);

        tab = tbTabs.newTabSpec("tab2");
        tab.setIndicator(getString(R.string.caption_tab_settings));
        tab.setContent(R.id.llSettings);
        tbTabs.addTab(tab);
    }

    /**
     * Initialize spinners on Settings-tab
     */
    private void initSpinners() {
        sHeight = (Spinner)findViewById(R.id.sHeight);
        sClothes = (Spinner)findViewById(R.id.sClothes);
        sActivity = (Spinner)findViewById(R.id.sActivity);

        ArrayAdapter<CharSequence> adapterHeight = ArrayAdapter.createFromResource(this,
                R.array.list_height, android.R.layout.simple_spinner_item);
        adapterHeight.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        ArrayAdapter<CharSequence> adapterClothes = ArrayAdapter.createFromResource(this,
                R.array.list_clothes, android.R.layout.simple_spinner_item);
        adapterClothes.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        ArrayAdapter<CharSequence> adapterActivity = ArrayAdapter.createFromResource(this,
                R.array.list_activity, android.R.layout.simple_spinner_item);
        adapterActivity.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sHeight.setAdapter(adapterHeight);
        sClothes.setAdapter(adapterClothes);
        sActivity.setAdapter(adapterActivity);
    }

    /**
     * Initialize listeners of GUI elements
     */
    private void initListeners() {
        sHeight.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                btnApply.setEnabled(true);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        sClothes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                btnApply.setEnabled(true);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        sActivity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                btnApply.setEnabled(true);
                TextView tvDescription = (TextView)findViewById(R.id.tvActivityDescription);
                switch (pos) {
                    case 0:
                        tvDescription.setText("Ruhezustand, bequem sitzend");
                        break;
                    case 1:
                        tvDescription.setText("Leichte Handarbeit (Schreiben, Tippen, Zeichnen, Nähen, " +
                                "Buchführung); Tätigkeit mit Hand und Arm;");
                        break;
                    default:
                        tvDescription.setText("");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    /**
     * Starting a thread to update data from the server
     */
    private void startUpdateThread() {
        mReadingThread = new Thread(new Runnable() {
            public void run() {
                Intent request = new Intent(NetworkService.BROADCAST_ACTION_RECEIVE);
                request.putExtra(Constants.EXTRA_PARAM_ACTION, Constants.COMMAND_GET_RAUM_TEMP);

                mRunning = true;

                while (mRunning) {
                    broadcastManager.sendBroadcast(request);

                    try {
                        Thread.sleep(Config.READING_INVERVALL);
                    } catch (InterruptedException e) {
                        // nothing
                    }
                }
            }
        });

        mReadingThread.start();
    }

    /**
     * Destroy: unregister broadcast receiver
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();

        mRunning = false;
        broadcastManager.unregisterReceiver(broadcastReceiver);
    }

    /**
     * OnPause
     */
    @Override
    protected void onPause() {
        super.onPause();
    }

    /**
     * OnResume
     */
    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * Local {@link BroadcastReceiver}
     */
    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getByteExtra(Constants.EXTRA_PARAM_ACTION, Constants.COMMAND_UNKNOWN) == Constants.COMMAND_SET_FEEL) {

                DialogUtils.dismissDialog();
                btnApply.setEnabled(false);

                if (intent.getByteExtra(Constants.EXTRA_REQ_RESULT, (byte) -1) == Constants.RESULT_OK) {
                    String showMessage = "Done successfully";

                    int rk_count = intent.getIntExtra(Constants.EXTRA_PARAM_CCNUMBER, 0);
                    for (int i = 0; i < rk_count; i++) {
                        double rk_before = intent.getDoubleExtra(String.format("rk_before%s", i), 0);
                        double rk_after = intent.getDoubleExtra(String.format("rk_after%s", i), 0);

                        if (rk_after == 0xFFFF) {
                            showMessage += String.format("\nRK %s: Wünsch nicht erfüllbar", (i + 1));
                        } else {
                            showMessage += String.format("\nRK %s: %s°C -> %s°C", (i + 1), String.valueOf(rk_before), String.valueOf(rk_after));
                        }
                    }

                    Toast.makeText(getBaseContext(), showMessage, Toast.LENGTH_LONG).show();

                    sbFeel.setProgress(50);

                } else if (intent.getByteExtra(Constants.EXTRA_REQ_RESULT, (byte) -1) == Constants.RESULT_NO_PERMISSIONS) {
                    Toast.makeText(getBaseContext(), "No permissions for operation", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getBaseContext(), "Error performing request", Toast.LENGTH_LONG).show();
                }
            } else {
                if (intent.getByteExtra(Constants.EXTRA_REQ_RESULT, (byte) -1) == Constants.RESULT_OK) {
                    float temp = intent.getFloatExtra(PLCVars.RAUM_TEMP, 0);
                    tvRaumTemp.setText(String.format(Config.FORMAT_STRING_TEMPERATUR, temp));
                } else {
                    tvRaumTemp.setText(Config.FORMAT_DEF_TEMPERATUR_VALUE);
                }
            }
        }
    };

    /**
     * Select zone by touching the person model
     */
    public boolean onTouch (View v, MotionEvent ev) {

        if (hotspots == null) {
            imgOverlay.setDrawingCacheEnabled(true);
            hotspots = Bitmap.createBitmap(imgOverlay.getDrawingCache());
            imgOverlay.setDrawingCacheEnabled(false);
        }

        final int action = ev.getAction();
        final int evX = (int) ev.getX();
        final int evY = (int) ev.getY();

        final String [] surfaces = getIntent().getStringArrayExtra("SURFACES");

        if (action == MotionEvent.ACTION_UP) {
            int touchColor = hotspots.getPixel(evX, evY);

            if (((BitmapDrawable) imgSelection.getDrawable()).getBitmap() != null) {
                ((BitmapDrawable) imgSelection.getDrawable()).getBitmap().recycle();
            }
            llSpinner.setVisibility(View.VISIBLE);

            int zoneIndex = getZoneIndexFromColor(touchColor);
            if (zoneIndex == 0) {
                imgSelection.setImageResource(R.drawable.model_bg);
                llSpinner.setVisibility(View.INVISIBLE);
                return true;
            }

            selectedIndex = getSurfaceIndex(surfaces, zoneIndex);

            final String surface = surfaces[selectedIndex];

            Canvas tempCanvas = null;
            Bitmap tempBitmap = null;

            for (String s: surface.split(",")) {
                try {
                    Bitmap bmp = null;
                    int val = Integer.valueOf(s.trim());

                    final BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 4;

                    if (val >= 0) {
                        bmp = BitmapFactory.decodeResource(getResources(), getResources().getIdentifier(String.format("model_%s", String.valueOf(val)), "drawable", this.getPackageName()), options);
                    } else {
                        bmp = BitmapFactory.decodeResource(getResources(), getResources().getIdentifier(String.format("model_%sb", String.valueOf(-val)), "drawable", this.getPackageName()), options);
                    }

                    if (tempCanvas == null) {
                        tempBitmap = bmp.copy(Bitmap.Config.ARGB_8888, true);
                        tempCanvas = new Canvas(tempBitmap);
                    }

                    final Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
                    tempCanvas.drawBitmap(bmp, 0, 0, paint);
                    bmp.recycle();
                } catch (Exception e) {
                    Log.e(Constants.LOG_TAG, e.getMessage());
                }
            }

            if (tempBitmap != null) {
                imgSelection.setImageDrawable(new BitmapDrawable(getResources(), tempBitmap));
            }

            sbFeel.setProgress(50);
        }
        return true;
    }

    /**
     * Returns zone index by color according to model_map.png
     *
     * @param color The color value as integer
     *
     * @return  Zone number [1..15] or 0 if color was not found
     */
    private int getZoneIndexFromColor(final Integer color) {
        if (ZONES_COLORS_FRONT.indexOf(color) != -1) {
            return ZONES_COLORS_FRONT.indexOf(color) + 1;
        }

        if (ZONES_COLORS_BACK.indexOf(color) != -1) {
            return -(ZONES_COLORS_BACK.indexOf(color) + 1);
        }

        return 0;
    }

    /**
     * Return surface string by zone index
     *
     * @param surfaces  The list of surfaces
     * @param zoneIndex The zone index
     *
     * @return
     */
    private int getSurfaceIndex(final String [] surfaces, final int zoneIndex) {

        int res = 0;

        for (String s: surfaces) {
            String [] elms = s.split(",");
            for (String e: elms) {
                if (Integer.valueOf(e) == zoneIndex) {
                    return res;
                }
            }
            res++;
        }

        return -1;
    }

    /**
     * Setup feel values
     */
    public void onApplyClicked(final View v) {
        DialogUtils.showWaitDialog(this, "Send data...");

        Intent request = new Intent(NetworkService.BROADCAST_ACTION_RECEIVE);
        request.putExtra(Constants.EXTRA_PARAM_ACTION, Constants.COMMAND_SET_FEEL);

        request.putExtra(Constants.EXTRA_PARAM_FEEL, feelValue);
        request.putExtra(Constants.EXTRA_PARAM_SELECTION, selectedIndex);
        request.putExtra(Constants.EXTRA_PARAM_HEIGHT, sHeight.getSelectedItemPosition());
        request.putExtra(Constants.EXTRA_PARAM_CLOTHES, sClothes.getSelectedItemPosition());
        request.putExtra(Constants.EXTRA_PARAM_ACTIVITY, sActivity.getSelectedItemPosition());

        broadcastManager.sendBroadcast(request);
    }
}

package de.fh_zwickau.raumcontex;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import de.fh_zwickau.raumcontex.de.fh_zwickau.raumcontex.common.Constants;
import de.fh_zwickau.raumcontex.network.NetworkService;
import de.fh_zwickau.raumcontex.de.fh_zwickau.raumcontex.common.PLCVars;
import de.fh_zwickau.raumcontex.de.fh_zwickau.raumcontex.common.DialogUtils;

/**
 * Expert activity: setup PID values
 *
 * Created by Alexandr Mitiaev on 22.09.2016.
 */
public class ExpertControlActivity extends AppCompatActivity {

    /**
     * The {@link LocalBroadcastManager} to receive data from the {@link NetworkService}
     */
    private LocalBroadcastManager mBroadcastManager;

    /**
     * Heizkreis 1 vorlauf sollwert
     */
    private EditText edHK1Sollwert;

    /**
     * Heizkreis 1 PID Ki-param
     */
    private EditText edHK1Ki;

    /**
     * Heizkreis 1 PID Kp-param
     */
    private EditText edHK1Kp;

    /**
     * Heizkreis 1 PID Kd-param
     */
    private EditText edHK1Kd;

    /**
     * Heizkreis 2 vorlauf sollwert
     */
    private EditText edHK2Sollwert;

    /**
     * Heizkreis 2 PID Ki-param
     */
    private EditText edHK2Ki;

    /**
     * Heizkreis 2 PID Kp-param
     */
    private EditText edHK2Kp;

    /**
     * Heizkreis 2 PID Kd-param
     */
    private EditText edHK2Kd;

    /**
     * Stein Speicher soll wert
     */
    private EditText edSSSollwert;

    /**
     * Hysterese value of the Stein Speicher
     */
    private EditText edSSHysterese;

    /**
     * Radio button "Heizen"
     */
    private RadioButton rbHeizen;

    /**
     * Radio button "Kuehlen"
     */
    private RadioButton rbKuehlen;

    /**
     * Apply-button
     */
    private Button btnApply;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expert_control);

        edHK1Sollwert = (EditText)findViewById(R.id.edHK1Sollwert);
        edHK1Ki = (EditText)findViewById(R.id.edHK1Ki);
        edHK1Kp = (EditText)findViewById(R.id.edHK1Kp);
        edHK1Kd = (EditText)findViewById(R.id.edHK1Kd);
        edHK2Sollwert = (EditText)findViewById(R.id.edHK2Sollwert);
        edHK2Ki = (EditText)findViewById(R.id.edHK2Ki);
        edHK2Kp = (EditText)findViewById(R.id.edHK2Kp);
        edHK2Kd = (EditText)findViewById(R.id.edHK2Kd);
        edSSSollwert = (EditText)findViewById(R.id.edSSSollwert);
        edSSHysterese = (EditText)findViewById(R.id.edSSHysterese);
        rbHeizen = (RadioButton)findViewById(R.id.rbHeizen);
        rbKuehlen = (RadioButton)findViewById(R.id.rbKuehlen);
        btnApply = (Button)findViewById(R.id.btnLogout);

        mBroadcastManager = LocalBroadcastManager.getInstance(this);
        mBroadcastManager.registerReceiver(broadcastReceiver, new IntentFilter(NetworkService.BROADCAST_ACTION_SEND));

        onUpdateClicked(null);
    }

    /**
     * Update data from server
     */
    public void onUpdateClicked(View v) {
        Intent request = new Intent(NetworkService.BROADCAST_ACTION_RECEIVE);
        request.putExtra(Constants.EXTRA_PARAM_ACTION, Constants.COMMAND_GET_INPUT_PARAMS);
        mBroadcastManager.sendBroadcast(request);

        DialogUtils.showWaitDialog(this, R.string.msg_get_data);
    }

    /**
     * Back button clicked
     */
    public void onBackClicked(View v) {
        startActivity(new Intent(this, ExpertActivity.class));
    }

    /**
     * Apply changes
     */
    public void onApplyClicked(View v) {

        DialogUtils.showWaitDialog(this, R.string.msg_save_data);

        try {
            Intent request = new Intent(NetworkService.BROADCAST_ACTION_RECEIVE);
            request.putExtra(Constants.EXTRA_PARAM_ACTION, Constants.COMMAND_SET_INPUT_PARAMS);

            request.putExtra(PLCVars.HK1_VORLAUF_SOLL, Float.valueOf(edHK1Sollwert.getText().toString()));
            request.putExtra(PLCVars.HK1_KI, Float.valueOf(edHK1Ki.getText().toString()));
            request.putExtra(PLCVars.HK1_KP, Float.valueOf(edHK1Kp.getText().toString()));
            request.putExtra(PLCVars.HK1_KD, Float.valueOf(edHK1Kd.getText().toString()));
            request.putExtra(PLCVars.HK2_VORLAUF_SOLL, Float.valueOf(edHK2Sollwert.getText().toString()));
            request.putExtra(PLCVars.HK2_KI, Float.valueOf(edHK2Ki.getText().toString()));
            request.putExtra(PLCVars.HK2_KP, Float.valueOf(edHK2Kp.getText().toString()));
            request.putExtra(PLCVars.HK2_KD, Float.valueOf(edHK2Kd.getText().toString()));
            request.putExtra(PLCVars.SS_VORLAUF_SOLL, Float.valueOf(edSSSollwert.getText().toString()));
            request.putExtra(PLCVars.SS_HYSTERESE, Float.valueOf(edSSHysterese.getText().toString()));
            request.putExtra(PLCVars.HEATING_MODE, rbHeizen.isChecked());

            mBroadcastManager.sendBroadcast(request);

            Toast.makeText(this, R.string.msg_data_saved, Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Toast.makeText(this, R.string.msg_error + e.getMessage(), Toast.LENGTH_LONG).show();
        } finally {
            DialogUtils.dismissDialog();
        }
    }

    /**
     * Destroy activity
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mBroadcastManager.unregisterReceiver(broadcastReceiver);
    }

    /**
     * Receive answer from the {@link NetworkService}
     */
    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getByteExtra(Constants.EXTRA_PARAM_ACTION, Constants.COMMAND_UNKNOWN) == Constants.COMMAND_GET_INPUT_PARAMS) {

                DialogUtils.dismissDialog();

                float val = intent.getFloatExtra(PLCVars.HK1_VORLAUF_SOLL, 0);
                edHK1Sollwert.setText(String.format("%.1f", val));

                val = intent.getFloatExtra(PLCVars.HK1_KI, 0);
                edHK1Ki.setText(String.format("%.1f", val));

                val = intent.getFloatExtra(PLCVars.HK1_KP, 0);
                edHK1Kp.setText(String.format("%.1f", val));

                val = intent.getFloatExtra(PLCVars.HK1_KD, 0);
                edHK1Kd.setText(String.format("%.1f", val));

                val = intent.getFloatExtra(PLCVars.HK2_VORLAUF_SOLL, 0);
                edHK2Sollwert.setText(String.format("%.1f", val));

                val = intent.getFloatExtra(PLCVars.HK2_KI, 0);
                edHK2Ki.setText(String.format("%.1f", val));

                val = intent.getFloatExtra(PLCVars.HK2_KP, 0);
                edHK2Kp.setText(String.format("%.1f", val));

                val = intent.getFloatExtra(PLCVars.HK2_KD, 0);
                edHK2Kd.setText(String.format("%.1f", val));

                val = intent.getFloatExtra(PLCVars.SS_VORLAUF_SOLL, 0);
                edSSSollwert.setText(String.format("%.1f", val));

                val = intent.getFloatExtra(PLCVars.SS_HYSTERESE, 0);
                edSSHysterese.setText(String.format("%.1f", val));

                boolean valb = intent.getBooleanExtra(PLCVars.HEATING_MODE, false);
                if (valb) {
                    rbHeizen.setChecked(true);
                } else {
                    rbKuehlen.setChecked(true);
                }

                btnApply.setEnabled(true);
            }
        }
    };
}

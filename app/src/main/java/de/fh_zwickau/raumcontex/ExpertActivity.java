package de.fh_zwickau.raumcontex;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import de.fh_zwickau.raumcontex.de.fh_zwickau.raumcontex.common.Config;
import de.fh_zwickau.raumcontex.de.fh_zwickau.raumcontex.common.Constants;
import de.fh_zwickau.raumcontex.network.NetworkService;
import de.fh_zwickau.raumcontex.de.fh_zwickau.raumcontex.common.PLCVars;
import de.fh_zwickau.raumcontex.de.fh_zwickau.raumcontex.common.DialogUtils;

/**
 * Expert activity: get information about actual and desired temperature,
 * control of heating circuits
 *
 * Created by Alexandr Mitiaev on 22.09.2016.
 */
public class ExpertActivity extends AppCompatActivity {
    /**
     * The {@link LocalBroadcastManager} to receive data from the {@link NetworkService}
     */
    private LocalBroadcastManager mBroadcastManager;

    /**
     * Raum temperatur
     */
    private TextView tvRaumTemp;

    /**
     * Mode: heizen/kuehlen
     */
    private TextView tvMode;

    /**
     * HK1 vorlauf
     */
    private TextView tvHK1Vorlauf;

    /**
     * HK2 vorlauf
     */
    private TextView tvHK2Vorlauf;

    /**
     * SS vorlauf
     */
    private TextView tvSSVorlauf;

    /**
     * HK1 vorlauf soll
     */
    private TextView tvHK1Soll;

    /**
     * HK2 vorlauf soll
     */
    private TextView tvHK2Soll;

    /**
     * StSp vorlauf soll
     */
    private TextView tvSSSoll;

    /**
     * Switch HK1 on/off
     */
    private Switch swHK1On;

    /**
     * Switch HK2 on/off
     */
    private Switch swHK2On;

    /**
     * Reading thread
     */
    private Thread mReadingThread;

    /**
     * Reading thread mRunning
     */
    private boolean mRunning;

    /**
     * Resume signal for reading thread
     */
    private Object signal = new Object();

    /**
     * Pause reading thread
     */
    private volatile boolean paused;

    /**
     * Start service and get the {@link LocalBroadcastManager}
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expert);

        tvRaumTemp = (TextView)findViewById(R.id.tvRaumTemp);
        tvMode = (TextView)findViewById(R.id.tvMode);
        tvHK1Vorlauf = (TextView)findViewById(R.id.tvHK1Vorlauf);
        tvHK2Vorlauf = (TextView)findViewById(R.id.tvHK2Vorlauf);
        tvSSVorlauf = (TextView)findViewById(R.id.tvSSVorlauf);
        tvHK1Soll = (TextView)findViewById(R.id.tvHK1Soll);
        tvHK2Soll = (TextView)findViewById(R.id.tvHK2Soll);
        tvSSSoll = (TextView)findViewById(R.id.tvSSSoll);
        swHK1On = (Switch)findViewById(R.id.swHK1On);
        swHK2On = (Switch)findViewById(R.id.swHK2On);

        mBroadcastManager = LocalBroadcastManager.getInstance(this);
        mBroadcastManager.registerReceiver(broadcastReceiver, new IntentFilter(NetworkService.BROADCAST_ACTION_SEND));

        // Start reading mReadingThread
        mReadingThread = new Thread(new Runnable() {
            public void run() {
                Intent request = new Intent(NetworkService.BROADCAST_ACTION_RECEIVE);
                request.putExtra(Constants.EXTRA_PARAM_ACTION, Constants.COMMAND_GET_SENSORS);

                mRunning = true;

                paused = false;

                while (mRunning) {
                    mBroadcastManager.sendBroadcast(request);

                    while (paused) {
                        synchronized (signal) {
                            try {
                                signal.wait();
                            } catch (InterruptedException e) {
                                // nothing
                            }
                        }
                    }

                    try {
                        Thread.sleep(Config.READING_INVERVALL);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        mReadingThread.start();
    }

    /**
     * Receive answer from the {@link NetworkService}
     */
    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            int cmdByte = intent.getByteExtra(Constants.EXTRA_PARAM_ACTION, Constants.COMMAND_UNKNOWN);

            if (cmdByte == Constants.COMMAND_GET_SENSORS) {
                if (intent.getByteExtra("result", (byte)0) != Constants.RESULT_OK) {
                    tvRaumTemp.setText(Config.FORMAT_DEF_TEMPERATUR_VALUE);
                    tvMode.setText("No permissions to read");
                    tvMode.setTextColor(Color.rgb(127, 127, 127));
                    tvHK1Vorlauf.setText(Config.FORMAT_DEF_TEMPERATUR_VALUE);
                    tvHK2Vorlauf.setText(Config.FORMAT_DEF_TEMPERATUR_VALUE);
                    tvSSVorlauf.setText(Config.FORMAT_DEF_TEMPERATUR_VALUE);
                    tvHK1Soll.setText(Config.FORMAT_DEF_TEMPERATUR_VALUE);
                    tvHK2Soll.setText(Config.FORMAT_DEF_TEMPERATUR_VALUE);
                    tvSSSoll.setText(Config.FORMAT_DEF_TEMPERATUR_VALUE);
                    return;
                }

                float val = intent.getFloatExtra(PLCVars.RAUM_TEMP, 0);
                tvRaumTemp.setText(String.format(Config.FORMAT_STRING_TEMPERATUR, val));

                boolean valb = intent.getBooleanExtra(PLCVars.HEATING_MODE, false);
                if (valb) {
                    tvMode.setText(getString(R.string.mode_hot));
                    tvMode.setTextColor(Color.rgb(255, 0, 0));
                } else {
                    tvMode.setText(getString(R.string.mode_cold));
                    tvMode.setTextColor(Color.rgb(0, 0, 255));
                }

                val = intent.getFloatExtra(PLCVars.HK1_VORLAUF, 0);
                tvHK1Vorlauf.setText(String.format(Config.FORMAT_STRING_TEMPERATUR, val));

                val = intent.getFloatExtra(PLCVars.HK2_VORLAUF, 0);
                tvHK2Vorlauf.setText(String.format(Config.FORMAT_STRING_TEMPERATUR, val));

                val = intent.getFloatExtra(PLCVars.SS_VORLAUF, 0);
                tvSSVorlauf.setText(String.format(Config.FORMAT_STRING_TEMPERATUR, val));

                val = intent.getFloatExtra(PLCVars.HK1_VORLAUF_SOLL, 0);
                tvHK1Soll.setText(String.format(Config.FORMAT_STRING_TEMPERATUR, val));

                val = intent.getFloatExtra(PLCVars.HK2_VORLAUF_SOLL, 0);
                tvHK2Soll.setText(String.format(Config.FORMAT_STRING_TEMPERATUR, val));

                val = intent.getFloatExtra(PLCVars.SS_VORLAUF_SOLL, 0);
                tvSSSoll.setText(String.format(Config.FORMAT_STRING_TEMPERATUR, val));

                valb = intent.getBooleanExtra(PLCVars.HK1_ON, false);
                swHK1On.setChecked(valb);

                valb = intent.getBooleanExtra(PLCVars.HK2_ON, false);
                swHK2On.setChecked(valb);
            } else if (cmdByte == Constants.COMMAND_HK1_CHANGE_STATE || cmdByte == Constants.COMMAND_HK2_CHANGE_STATE) {
                if (intent.getByteExtra("result", (byte)1) != (byte)1) {
                    Toast.makeText(getBaseContext(), "No permissions for changing the state", Toast.LENGTH_LONG).show();
                }
            }
        }
    };

    /**
     * "Control"-button clicked event
     *
     * @param v
     */
    public void onControlClicked(View v) {
        startActivity(new Intent(this, ExpertControlActivity.class));
    }

    /**
     * Disconnect button clicked
     *
     * @param v
     */
    public void onDisconnectClicked(View v) {
        startActivity(new Intent(this, LoginActivity.class));
    }

    /**
     * Change state of HK1/HK2
     *
     * @param v
     */
    public void onHKSwitchClicked(final View v) {
        DialogUtils.showConfirmDialog(this, R.string.confirm_title, R.string.msg_change_hkstate,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();

                        Intent request = new Intent(NetworkService.BROADCAST_ACTION_RECEIVE);

                        if (v.getId() == R.id.swHK1On) {
                            request.putExtra(Constants.EXTRA_PARAM_ACTION, Constants.COMMAND_HK1_CHANGE_STATE);
                        } else {
                            request.putExtra(Constants.EXTRA_PARAM_ACTION, Constants.COMMAND_HK2_CHANGE_STATE);
                        }

                        mBroadcastManager.sendBroadcast(request);
                    }
                }, null, R.string.msg_yes, R.string.msg_no);
    }

    /**
     * Resume activity
     */
    @Override
    protected void onResume() {
        super.onResume();

        paused = false;
        synchronized (signal) {
            signal.notify();
        }
    }

    /**
     * Pause activity
     */
    @Override
    public void onPause() {
        super.onPause();
        paused = true;
    }

    /**
     * Destroy activity: unregister broadcast manager and stop network service
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();

        mRunning = false;
        mBroadcastManager.unregisterReceiver(broadcastReceiver);
    }
}
